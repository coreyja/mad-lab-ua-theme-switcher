jQuery(document).ready(function() {
			jQuery("#" + jQuery('input[name="mlmg_ua_iphone_theme"]').val().replace("/","\\/") + " div.iPhone").addClass("selected");
			jQuery("#" + jQuery('input[name="mlmg_ua_ipad_theme"]').val().replace("/","\\/") + " div.iPad").addClass("selected");
			jQuery("#" + jQuery('input[name="mlmg_ua_mobile_theme"]').val().replace("/","\\/") + " div.Mobile").addClass("selected");
			//Find the currently selected themes and add the selected class.
			
			jQuery('div.choiceButton').click(function() {
				if (jQuery(this).hasClass('iPhone')){
					jQuery("#" + jQuery('input[name="mlmg_ua_iphone_theme"]').val().replace("/","\\/") + " div.iPhone").removeClass("selected");
					var parentID = jQuery(this).parents("div.mobile-theme").attr('id');
					jQuery('input[name="mlmg_ua_iphone_theme"]').val(parentID);
				}
				if (jQuery(this).hasClass('iPad')){
					jQuery("#" + jQuery('input[name="mlmg_ua_ipad_theme"]').val().replace("/","\\/") + " div.iPad").removeClass("selected");
					var parentID = jQuery(this).parents("div.mobile-theme").attr('id');
					jQuery('input[name="mlmg_ua_ipad_theme"]').val(parentID);
				}
				if (jQuery(this).hasClass('Mobile')){
					jQuery("#" + jQuery('input[name="mlmg_ua_mobile_theme"]').val().replace("/","\\/") + " div.Mobile").removeClass("selected");
					var parentID = jQuery(this).parents("div.mobile-theme").attr('id');
					jQuery('input[name="mlmg_ua_mobile_theme"]').val(parentID);
				}
				
				jQuery(this).addClass("selected");
				
				//On click update the hidden field with the correct theme and add selected to this class for displaying nicely.
			});
		});