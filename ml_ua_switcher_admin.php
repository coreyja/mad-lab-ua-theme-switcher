<?php

function mlmg_admin_menu(){
	add_options_page('UserAgent Theme Switcher','UserAgent Theme Switcher','manage_options','mlmg_ua_options','mlmg_ua_admin_page');
}

function mlmg_ua_admin_page() {

	//must check that the user has the required capability 
    if (!current_user_can('manage_options'))
    {
      wp_die( __('You do not have sufficient permissions to access this page.') );
    }
	
	$hidden_field = 'mlmg_submit_hidden';
	
	if ( isset($_POST[$hidden_field]) && $_POST[$hidden_field] == 'Y'){
		//Save all the options that were just uploaded and show success mesage
		update_option( 'mlmg_ua_ipad_theme' , $_POST['mlmg_ua_ipad_theme']);
		update_option( 'mlmg_ua_iphone_theme' , $_POST['mlmg_ua_iphone_theme']);
		update_option( 'mlmg_ua_mobile_theme' , $_POST['mlmg_ua_mobile_theme']);		
		echo '<div class="updated"><p><strong>Options Saved</strong></p></div>';
	}
	
	$ipad_theme = get_option('mlmg_ua_ipad_theme');
	$iphone_theme = get_option('mlmg_ua_iphone_theme');
	$mobile_theme = get_option('mlmg_ua_mobile_theme');
	
	
	
	echo '<div class="wrap">';
	echo '<h2>UserAgent Theme Switcher Settings</h2>';
?>
		<form method="post" action="">
			<input type="hidden" name="<?php echo $hidden_field; ?>" value="Y">
			
			<input type="hidden" name="mlmg_ua_iphone_theme" value="<?php echo $iphone_theme; ?>">
			<input type="hidden" name="mlmg_ua_ipad_theme" value="<?php echo $ipad_theme; ?>">
			<input type="hidden" name="mlmg_ua_mobile_theme" value="<?php echo $mobile_theme; ?>">

			
		<?php 
		$themes = wp_get_themes();
		foreach ($themes as $theme){
			if (contains('/wp-content/themes/mobile',$theme->get_stylesheet_directory())){
				//Only display themes that are in the mobile subfolder of themes.
				//This allows users to seperate their mobile and "normal" themes

				$screenshot = $theme->get_screenshot(); ?>
				<div class="mobile-theme" id="<?php echo $theme->get_stylesheet(); ?>">
					<img src="<?php echo $screenshot; ?>">
					<h3><?php echo $theme->display('Name'); ?></h3>
					<p>Author: <?php echo $theme->display('Author'); ?></p>
					<div class="iPhone choiceButton">iPhone</div>
					<div class="iPad choiceButton">iPad</div>
					<div class="Mobile choiceButton">Mobile</div>
				</div>
			
			<?php			
			}
		}
		?>
		
		<br class="clear" />
		
		<p class="submit">
				<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
			</p>

		</form>
	</div>
<?php
}

function ml_ua_switcher_css() {
	wp_enqueue_style('ml_ua_switcher_admin_css',plugins_url('style.css',__FILE__));
	wp_enqueue_script('ml_ua_switcher_admin_js',plugins_url('admin.js',__FILE__));

}
?>