<?php
/*
Plugin Name: UserAgent Switcher
Plugin URI: http:/www.google.com
Description: Allows changing the current theme based on the browsers User Agent
Version: 0.1
Author: Corey Alexander
*/

require_once('ml_ua_switcher_admin.php');


if ( is_admin() ){
	//Registed the admin menu and pages. Also enque the css that the admin page uses
	add_action('admin_menu', 'mlmg_admin_menu');
	add_action('admin_enqueue_scripts','ml_ua_switcher_css');
} else {
	// Only do these if you aren't on an admin page
	// No reason to waste time with these on all pages
	update_option('mlmg_current_theme', get_template());
	add_filter( 'template', 'useragent_template' );
	add_filter( 'stylesheet', 'useragent_template' );
	add_action('wp_enqueue_scripts','mlmg_ua_jquerymobile');

	//Create the shortcodes
	add_shortcode('mlmg-ua-footer-link', 'mlmg_ua_footer_link');
	add_shortcode('mlmg-ua-fullsite-url','mlmg_ua_fullsite_url');
	add_shortcode('mlmg-ua-mobile-url','mlmg_ua_mobile_url');
	add_shortcode('mlmg-ua-iphone-url','mlmg_ua_iphone_url');
	add_shortcode('mlmg-ua-ipad-url','mlmg_ua_ipad_url');
}


function useragent_template(){
	
	update_option('mlmg_ua_on_mobile',False);
	update_option('mlmg_ua_forced_full',False);
	//Default both options to False. We will set the neccesay ones below.

	$ipad = get_option('mlmg_ua_ipad_theme');
	$iphone = get_option('mlmg_ua_iphone_theme');
	$mobile = get_option('mlmg_ua_mobile_theme');
	$full = get_option('mlmg_current_theme');

	if (isset($_GET['mobile'])){
		if (!headers_sent()) setcookie('mlmg-useragent','mobile',0); //If can only set cookies if the headers haven't been sent yet
		update_option('mlmg_ua_on_mobile',True);
		return $mobile;
	}
	
	if (isset($_GET['ipad'])){
		if (!headers_sent()) setcookie('mlmg-useragent','ipad',0);
		update_option('mlmg_ua_on_mobile',True);
		return $ipad;
	}
	
	if (isset($_GET['iphone'])){
		if (!headers_sent()) setcookie('mlmg-useragent','iphone',0);
		update_option('mlmg_ua_on_mobile',True);
		return $iphone;
	}
	
	if (isset($_GET['fullsite'])){
		if (!headers_sent()) setcookie('mlmg-useragent','full',0);
		update_option('mlmg_ua_forced_full',True);
		return $full; 
	}
	
	if (!isset($_GET['default'])){
		//If default isn't set we will use what the user explicitly asked for before, or the default for that UA
		if (isset($_COOKIE['mlmg-useragent'])){
			//If the cookie isn't set we will retrieve the user agent to check against that below.
			if ($_COOKIE['mlmg-useragent'] == 'mobile'){
				update_option('mlmg_ua_on_mobile',True);
				return $mobile;
			}
			if ($_COOKIE['mlmg-useragent'] == 'ipad'){
				update_option('mlmg_ua_on_mobile',True);
				return $ipad;
			}
			if ($_COOKIE['mlmg-useragent'] == 'iphone'){
				update_option('mlmg_ua_on_mobile',True);
				return $iphone;
			}
			if ($_COOKIE['mlmg-useragent'] == 'full'){
				update_option('mlmg_ua_forced_full',True);
				return $full;
			}
		}
	} else {
		//Default is set, so invalidate the user agent cookie.
		//This will force it to check the user agent and display the correct theme based on that.
		if (!headers_sent()) setcookie('mlmg-useragent','',-1);
	}
	
	
	
	$useragent=$_SERVER['HTTP_USER_AGENT'];

	if ( contains('ipad',$useragent)) {
		//All iPads have iPad in the User Agent
		// TODO Determine if tablets should display iPad instead of mobile. And how to check for tablets, if possible
		update_option('mlmg_ua_on_mobile',True);
		return $ipad;
	}
	if ( contains('iphone',$useragent)) {
		//All iPhones have iphone in the User Agent
		update_option('mlmg_ua_on_mobile',True);
		return $iphone;
	}
	if ( preg_match('/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
		//This Reg Exp was found online for checking for mobile. Will need to be updated periodicaly to keep up with new phones and standards.
		update_option('mlmg_ua_on_mobile',True);
		return $mobile;
	}
	
	return $full;
	
	
}

/**
* Checks to see of a string contains a particular substring
* @param $substring the substring to match
* @param $string the string to search 
* @return true if $substring is found in $string, false otherwise
*/
function contains($substring, $string) {
        $pos = stripos($string, $substring);
 
        if($pos === false) {
                // string needle NOT found in haystack
                return false;
        }
        else {
                // string needle found in haystack
                return true;
        }
 
}

function mlmg_ua_jquerymobile(){
	if (get_option('mlmg_ua_on_mobile')){

		//Enqueue jQuery Mobile CSS
		wp_deregister_style( 'jquery_mobile_css' );
		wp_enqueue_style( 'jquery_mobile_css', 'http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.css');
	
		//Enqueue jQuery that matches the JQuery Mobile
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 'http://code.jquery.com/jquery-1.7.1.min.js');
		wp_enqueue_script( 'jquery' );
	
		//Enqueue jQuery Mobile
		wp_deregister_script( 'jquery_mobile' );
		wp_enqueue_script( 'jquery_mobile', 'http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.js');
		
		
	}
}

function mlmg_ua_footer_link(){
	//This will be called when a user uses the [mlmg-ua-footer-link] short code
	if (get_option('mlmg_ua_on_mobile')){
		return '<a href="?fullsite" class="mlmg_ua_footer_link">View Full Site</a>';
	}
	if (get_option('mlmg_ua_forced_full')) {
		return '<a href="?default" class="mlmg_ua_footer_link">View Mobile Site</a>';
	}
}


//The following doesn't really need a shortcode or anything.
//But for user ease. As well as ability to change the url structure in the future, this shortcode makes sense.
//No reason for the user to know the URL structure we use
function mlmg_ua_fullsite_url(){
	return "?fullsite";
	
}
function mlmg_ua_mobile_url(){
	return "?mobile";
}
function mlmg_ua_iphone_url(){
	return "?mobile";
}
function mlmg_ua_ipad_url(){
	return "?mobile";
}

 ?>